package ru.thever4.lessons.fourth.screens

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import ru.thever4.lessons.fourth.adapters.TodayAdapter
import ru.thever4.lessons.fourth.data.Day
import ru.thever4.lessons.fourth.data.Days
import ru.thever4.lessons.fourth.data.ScheduleDataStorage
import ru.thever4.lessons.fourth.databinding.FragmentTodayBinding
import ru.thever4.lessons.fourth.databinding.FragmentWeekBinding
import java.time.DayOfWeek
import java.time.LocalDate

class TodayFragment : Fragment() {

    private var binding: FragmentTodayBinding? = null
    private var adapter: TodayAdapter = TodayAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTodayBinding.inflate(inflater, container, false)
        return binding?.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        with(binding?.rViewToday) {
            this ?: return
            layoutManager = LinearLayoutManager(requireContext())
            adapter = this@TodayFragment.adapter
        }

        val day = getDayOfWeekSchedule(ScheduleDataStorage.scheduleFirstWeek)

        if (day != null){
            adapter.submitList(day.lessons)
            binding?.dayInfo?.text = day.ofWeek.toString()
        }
        else{
            binding?.dayInfo?.text = "Сегодня пар нет"
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun getDayOfWeekSchedule(schedule: List<Day>): Day? {
        val currentDayOfWeek = when (LocalDate.now().dayOfWeek) {
            DayOfWeek.MONDAY -> Days.MONDAY
            DayOfWeek.TUESDAY -> Days.TUESDAY
            DayOfWeek.WEDNESDAY -> Days.WEDNESDAY
            DayOfWeek.THURSDAY -> Days.THURSDAY
            DayOfWeek.FRIDAY -> Days.FRIDAY
            DayOfWeek.SATURDAY -> Days.SATURDAY
            DayOfWeek.SUNDAY -> Days.SUNDAY
        }
        return schedule.find { it.ofWeek == currentDayOfWeek }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            TodayFragment().apply {

            }
    }
}