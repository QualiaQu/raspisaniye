package ru.thever4.lessons.fourth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ru.thever4.lessons.fourth.databinding.ActivityMainBinding
import ru.thever4.lessons.fourth.screens.MainFragment

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        supportFragmentManager.beginTransaction()
            .replace(binding.fragmentRootLayout.id, MainFragment.newInstance())
            .commit()
    }
}