package ru.thever4.lessons.fourth.data

data class Day(
    val ofWeek: Days,
    val lessons: List<Lesson>
)